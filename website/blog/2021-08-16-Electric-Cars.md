---
title: Rise of Electric Cars
author: Tomo Hishinuma
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

After the last century dominated by gasoline powered cars, the car industry has finally revolutionized to a more sustainable power source. The electric car has had the spotlight in recent years as more people have been shifting a want towards a better alternative.
