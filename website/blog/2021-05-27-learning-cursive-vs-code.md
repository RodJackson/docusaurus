---
title: Learning Cursive vs. Code
author: MaKayla Emahiser
authorURL: http://twitter.com/
authorFBID: 100002976521003
---
In a time where pencils are being exchanged for keyboards, and curly que writing exchanged for code. One heroic young kid must overcome a battle against time. Charlie Codesalot is faced with a challenge - he must translate this archaic, old timey language into an effective code to ensure his message is translated perfectly for his teams success. Through trials and obstacles, Charlie is able to decipher the cursive and turn it into a beautiful application. Follow along for more Charlie Codesalot adventures. 
