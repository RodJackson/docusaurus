---
title: Fall Beginnings
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

It was fall in North Carolina. The leaves were changing.

All this reminded me of a friend that I have. I'm hopeful for the future.
