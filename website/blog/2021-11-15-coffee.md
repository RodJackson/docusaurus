---
title: Coffee or Tea, drink responsibly!
author: Randeep Nag
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Sometimes, our parents and trusted adults need a boost to get through the day. 

They don't have the energy that children do, and can be grumpy in the mornings. 

Coffee and tea are hot beverages that help adults feel functional. Be careful - start slow because you don't want to rely on them. 
